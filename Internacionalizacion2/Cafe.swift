//
//  Cafe.swift
//  Internacionalizacion2
//
//  Created by Salvador Lopez on 26/06/23.
//

import Foundation

// Definir la interfaz del objecto y las funcionalidades basicas.
protocol Cafe {
    func getDescription() -> String
    func getCost() -> Double
}
