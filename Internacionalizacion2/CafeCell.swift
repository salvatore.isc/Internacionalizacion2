//
//  CafeCell.swift
//  Internacionalizacion2
//
//  Created by Salvador Lopez on 26/06/23.
//

import UIKit

class CafeCell: UITableViewCell {
    
    @IBOutlet weak var descriptionLb: UILabel!
    @IBOutlet weak var costLb: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
