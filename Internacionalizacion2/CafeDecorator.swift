//
//  CafeDecorator.swift
//  Internacionalizacion2
//
//  Created by Salvador Lopez on 26/06/23.
//

import Foundation

/// Decorator: Implementa el mismo protocolo que el componente y contiene una referecia a un objeto componente.
/// Agrega funcionalidad adicional.

class CafeDecorator: Cafe {
    
    private let cafe: Cafe
    
    init(cafe: Cafe){
        self.cafe = cafe
    }
    
    func getDescription() -> String {
        return cafe.getDescription()
    }
    
    func getCost() -> Double {
        return cafe.getCost()
    }
    
}
