//
//  CafeList.swift
//  Internacionalizacion2
//
//  Created by Salvador Lopez on 26/06/23.
//

import Foundation

let cappuccinoCoffe = CappuccinoCafe() // Obj original
let capuuccinoVanilla = ExtraVanillaDecorator(cafe: cappuccinoCoffe) // Extra Vainilla
let capuccinoVanillaAndCaramel = ExtraCaramelDecorator(cafe: capuuccinoVanilla) // Extra Vainilla y Caramelo


struct Cafes {
    static let cafes: [Cafe] = [cappuccinoCoffe,capuuccinoVanilla,capuccinoVanillaAndCaramel]
}
