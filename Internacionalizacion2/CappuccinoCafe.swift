//
//  CappuccinoCafe.swift
//  Internacionalizacion2
//
//  Created by Salvador Lopez on 26/06/23.
//

import Foundation

class CappuccinoCafe: Cafe{
    
    func getDescription() -> String {
        return NSLocalizedString("Simple_Cappuccino_Description", comment: "Coffe description")
    }
    
    func getCost() -> Double {
        let costString = NSLocalizedString("Simple_Cappuccino_Cost", comment: "Coffe cost")
        return Double(costString) ?? 0
    }
}
