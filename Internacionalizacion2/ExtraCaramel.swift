//
//  ExtraCaramel.swift
//  Internacionalizacion2
//
//  Created by Salvador Lopez on 26/06/23.
//

import Foundation

class ExtraCaramelDecorator: CafeDecorator{
    
    override func getDescription() -> String {
        return super.getDescription() + NSLocalizedString("Extra_Caramel", comment: "extra ingredient")
    }
    
    override func getCost() -> Double {
        return super.getCost() + 2.00
    }
    
}
