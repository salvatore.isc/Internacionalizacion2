//
//  MyAlertController.swift
//  Internacionalizacion2
//
//  Created by Salvador Lopez on 26/06/23.
//

import UIKit

class AlertController {
    static func showAlert(title: String, message: String, butttons:[String], completionHandler: ((Int) -> Void)?){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for (index, titulo) in butttons.enumerated() {
            let action = UIAlertAction(title: titulo, style: .default) {
                _ in
                completionHandler?(index)
            }
            alertController.addAction(action)
        }
        
        DispatchQueue.main.async {
            UIApplication.shared.keyWindow?.rootViewController?.present(alertController, animated: true, completion: nil)
        }
        
    }
}
