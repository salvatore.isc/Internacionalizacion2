//
//  Practica.swift
//  Internacionalizacion2
//
//  Created by Salvador Lopez on 26/06/23.
//

import Foundation

/**
 
 Instrucciones:
    Realizar la creacion de las clases correspondientes a una lista de productos, y mostrarlos en una tabla.
 
 Considerar:
    - Hacer uso de patron de diseño Decorator para que los articulos contengan diferentes atributos: por ejemplo, Tamaño, Talla, Color, etc. (5 propiedades).
    - Realizar la parte grafica a consideración:  SwiftUI ó Storyboard.
    - Tenga soporte para al menos 2 idiomas.
    - Crear eventos para los articulos listados y mostar detalle de agregar al carrito con un Alert.
 
 */
