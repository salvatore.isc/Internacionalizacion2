//
//  ViewController.swift
//  Internacionalizacion2
//
//  Created by Salvador Lopez on 26/06/23.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // Part 1 Simple Class
        //var simpleCappucino = CappuccinoCafe()
        //print(simpleCappucino.getDescription())
        //print(simpleCappucino.getCost())
        
        // Part 2 Decorator
        //Uso del patron decorador
        //var cappuccinoVanilla: Cafe = CappuccinoCafe() // Objeto original
        //cappuccinoVanilla = ExtraVanillaDecorator(cafe: cappuccinoVanilla) //Decorador que agrega Vainilla extra.
        
        //var cappuccinoCaramel: Cafe = CappuccinoCafe()
        //cappuccinoCaramel = ExtraCaramelDecorator(cafe: cappuccinoCaramel) //Decorador que agrega Caramelo extra.
        
        //print(cappuccinoVanilla.getDescription())
        //print(cappuccinoVanilla.getCost())
        
        //print(cappuccinoCaramel.getDescription())
        //print(cappuccinoCaramel.getCost())
        
        // Part 3
        //Obtener desc y cost de acada elemento en la Struct
        for coffe in Cafes.cafes {
            print(coffe.getDescription())
            print(coffe.getCost())
            print("---")
        }
        
        tableView.delegate = self
        tableView.dataSource = self
        
        let name = "Roger"
        let greet = String(format: NSLocalizedString("greet", comment: "just greeting"), name)
        print(greet)
        
    }


}

extension ViewController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        Cafes.cafes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath) as! CafeCell
        
        cell.descriptionLb.text = Cafes.cafes[indexPath.row].getDescription()
        cell.costLb.text = String(String(format: "%.2f", Cafes.cafes[indexPath.row].getCost()))
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return NSLocalizedString("Table_List_Name", comment: "Nombre de la table de los cafes")
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        AlertController.showAlert(title: NSLocalizedString("Add_Alert", comment: "Titulo de la alerta en Agregar"), message: NSLocalizedString("Add_Message", comment: "Add Message text"), butttons: [NSLocalizedString("OK", comment: "OK"),NSLocalizedString("CANCEL", comment: "CANCEL")]) { index in
            if index == 0 {
                print("Agregar al carrito de compra.")
            }else{
                print("El usuario no esta interesado.")
            }
        }
    }
    
}
